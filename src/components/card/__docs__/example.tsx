import React, { FC } from "react";
import Card, { CardProps } from "../card";

const Example: FC<CardProps> = ({
  onClick = () => {},
  title = 'My Card',
  children
}) => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Card
        title={title}
        children={children}
        onClick={onClick}
      />
    </div>
  );
};

export default Example;
