import React, { MouseEventHandler } from "react";
import styled from "styled-components";

export type CardProps = {
  title?: string;
  children: React.ReactNode;
  onClick?: MouseEventHandler<HTMLDivElement>;
};

const StyledCard = styled.div`
  display: flex;
  flex-direction: column;
  color: ${(props) => props.theme.colors.white};
  font-size: ${props => props.theme.fontSizes.medium};
  background-color: ${(props) => props.theme.colors.darkBlue};
  border: 1px solid ${props => props.theme.colors.lightBlue};
  padding: ${(props) => props.theme.spacing.s4};
  box-shadow: 0 ${props => props.theme.spacing['s0-5']} ${props => props.theme.spacing.s1} ${props => props.theme.colors.black};
  font-family: ${props => props.theme.fonts.primary};

  h1 {
    margin-bottom: ${props => props.theme.spacing.s4}
  }
`;

const Card: React.FC<CardProps> = ({ children, title, onClick, ...props }) => {
  return (
    <StyledCard {...props} onClick={onClick}>
      <h1>
        {title}
      </h1>
      {children}
    </StyledCard>
  );
};

export default Card;
