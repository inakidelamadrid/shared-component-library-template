export const theme = {
    colors: {
      primary: '#0096c7',
      darkBlue: '#023E8A',
      lightBlue: '#48CAE4',
      veryLightBlue: '#90E0EF',
      secondary: '#480ca8',
      white: '#FFFFFF',
      black: '#000000',
    },
    fonts: {
      primary: "'Inter', sans-serif",
      secondary: "'Roboto', sans-serif",
    },
    fontSizes: {
      small: '12px',
      medium: '16px',
      large: '24px',
      xlarge: '32px',
    },
    spacing: {
      's0-5': '0.125rem',
      's1': '0.25rem',
      's2': '0.5rem',
      's3': '0.75rem',
      's4': '1rem'
    }
  };